/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresionesregulares;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExpresionesRegulares {    

    static java.util.Scanner teclado = new java.util.Scanner(System.in);
    
    public static void main(String[] args) {        
        String input = "";
        do{
            System.out.println("************************************");
            System.out.println("*  1. Empieza por a                *");
            System.out.println("*  2. Empieza por a y acaba [0-9]  *");
            System.out.println("*  3. No empieza por $ ni dígito   *");
            System.out.println("*  4. No contiene abc              *");
            System.out.println("*  5. No contiene 10-99            *");
            System.out.println("*  6. Licencia válida              *");
            System.out.println("*  7. Palabra letras 5-10          *");
            System.out.println("*  8. Array matrículas             *");
            System.out.println("*  9. DNI                          *");
            System.out.println("* 10. Es doc(x)                    *");
            System.out.println("* 11a.Vocales ord. sin repetición  *");
            System.out.println("* 11b.Vocales ord. sin repetición  *");
            System.out.println("* 12. Código producto              *");
            System.out.println("* 13. Termina en punto             *");
            System.out.println("* 14. Reemplazar números 4 cifras  *");
            System.out.println("* 15. Es posición válida           *");
            System.out.println("* 16. Reemplazar no alfanuméricos  *");
            System.out.println("* 17. Formato fecha                *");
            System.out.println("* 18. Limpiar cadena               *");
            System.out.println("* 19. Comprobar IP                 *");
            System.out.println("* 20. Limpiar IP                   *");
            System.out.println("************************************");
            input = teclado.nextLine();
            String cadena;
            switch(input){                
                case "1":                    
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(empiezaPorA(cadena));
                    break;
                case "2":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(empiezaPorAYAcabaNumero(cadena));
                    break;
                case "3":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(noEmpiezaPor$NiNum(cadena));
                    break;
                case "4":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(noContieneAbc(cadena));
                    break;
                case "5":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(noContiene10_99(cadena));
                    break;
                case "6":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(licenciaValida(cadena));
                    break;
                case "7":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(palabraLetras5_10(cadena));
                    break;
                case "8":
                    System.out.println("Inserte las matrículas a comprobar");
                    cadena = teclado.nextLine();
                    String[] matriculas = cadena.split(" +");
                    for(String m : matriculas){
                        System.out.printf("La matrícula %s es válida: %b\n", m, esMatricula(m));
                    }
                    break;
                case "9":
                    System.out.println("Inserte los DNIs a comprobar");
                    cadena = teclado.nextLine();
                    String[] dnis = cadena.split(" +");
                    for(String d : dnis){
                        System.out.printf("El DNI %s es válido: %b\n", d, esDNI(d));
                    }
                    break;
                case "10":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(esDocx(cadena));
                    break;
                case "11a":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(vocalesOrdenadasSinRepeticion(cadena));
                    break;
                case "11b":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(vocalesOrdenadasConRepeticion(cadena));
                    break;
                case "12":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(esCodigoProducto(cadena));
                    break;
                case "13":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(terminaEnPunto(cadena));
                    break;
                case "14":
                    System.out.println("Inserte la cadena a reemplazar");
                    cadena = teclado.nextLine();
                    System.out.println(reemplazarNum4Cifras(cadena));
                    break;
                case "15":
                    System.out.println("Inserte la cadena a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(esPosicionValida(cadena));
                    break;
                case "16":
                    System.out.println("Inserte la cadena a reemplazar");
                    cadena = teclado.nextLine();
                    System.out.println(reemplazarNoAlfanumericos(cadena));
                    break;
                case "17":
                    System.out.println("Inserte la fecha a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(esFormatoFecha(cadena));
                    break;
                case "18":
                    System.out.println("Inserte la cadena a limpiar");
                    cadena = teclado.nextLine();
                    System.out.println(limpiarCadena(cadena));
                    break;
                case "19":
                    System.out.println("Inserte la IP a comprobar");
                    cadena = teclado.nextLine();
                    System.out.println(esIP(cadena));
                    break;
                case "20":
                    System.out.println("Inserte la IP a limpiar");                    
                    cadena = teclado.nextLine();
                    if(esIP(cadena)){
                        System.out.println(limpiarIP(cadena));
                    }
                    else{
                        System.out.println("No es una IP válida");
                    }
                    break;
            }
        }
        while(!input.equals("0"));
        
    }
    
    private static boolean empiezaPorA(String input){
        
        Pattern p = Pattern.compile("^a");
        Matcher m = p.matcher(input);
        return m.find();
        /*
        Otra opción
        
        Pattern p = Pattern.compile("^a.*");
        Matcher m = p.matcher(input);
        return m.matches();*/
    }
     
    private static boolean empiezaPorAYAcabaNumero(String s){
        Pattern p = Pattern.compile("^a.*[0-9]$");
        Matcher m = p.matcher(s);
        return m.matches();
        
        /*
        Otra forma
        
        Pattern p = Pattern.compile("^a.*[0-9]$");
        Matcher m = p.matcher(s);
        return m.find();*/
    }
    
    private static boolean noEmpiezaPor$NiNum(String s){
        //return Pattern.compile("^[^\\$0-9].*").matcher(s).matches();        
        Pattern p = Pattern.compile("^[^\\$0-9].*");        
        Matcher m = p.matcher(s);
        return m.matches();
    }
    
    private static boolean noContieneAbc(String s){
        Pattern p = Pattern.compile("\\b[abc]{3}\\b");
        Matcher m = p.matcher(s);
        if(m.find()){
            return false;
        }
        else{
            return true;
        }
        //return !m.find();
    }
    
    private static boolean noContiene10_99(String s){
        Pattern p = Pattern.compile("\\b[1-9][0-9]\\b");
        Matcher m = p.matcher(s);
        return !m.find();
    }
    
    private static boolean licenciaValida(String s){
        return Pattern.matches("\\d{4}[A-Z]{2}", s);
        /*Pattern p = Pattern.compile("\\d{4}[A-Z]");        
        Matcher m = p.matcher(s);
        return m.matches();*/
    }
    
    private static boolean palabraLetras5_10(String s){
        Pattern p = Pattern.compile("\\b[A-Za-z]{5,10}\\b");
        Matcher m = p.matcher(s);
        return m.find();
    }
    
    private static boolean esMatricula(String s){
        Pattern p = Pattern.compile("[0-9]{4}[A-Z]{3}");
        Matcher m = p.matcher(s);
        return m.matches();
    }
    
     private static boolean esDNI(String s){
        Pattern p = Pattern.compile("[0-9]{7,8}[A-Z]");
        Matcher m = p.matcher(s);
        return m.matches();
    }
     
     private static boolean esDocx(String s){
         Pattern p = Pattern.compile("\\.doc(x)?$");
         Matcher m = p.matcher(s);
         return m.find();
     }
     
     private static boolean vocalesOrdenadasSinRepeticion(String s){
         return Pattern.matches("[^aeiou]*a[^aeiou]*e[^aeiou]*i[^aeiou]*o[^aeiou]*u[^aeiou]*", s);
     }
     
     private static boolean vocalesOrdenadasConRepeticion(String s){
         return Pattern.matches("[^aeiou]*a[^eiou]*e[^aiou]*i[^aeou]*o[^aeiu]*u[^aeio]*", s);
     }
     
     private static boolean esCodigoProducto(String s){
         return Pattern.matches("[A-Z]{3}[0-9]{3}[AEOFM]", s);
     }
     
    private static boolean terminaEnPunto(String s){
        Pattern p = Pattern.compile("\\.$");
        Matcher m = p.matcher(s);
        return m.find();
    }
    
    private static String reemplazarNum4Cifras(String s){
        Pattern p = Pattern.compile("\\b[0-9]{4}\\b");
        Matcher m = p.matcher(s);
        return m.replaceAll("****");
    }
    
    private static boolean esPosicionValida(String s){
        return Pattern.matches("[A-Z]{4,5}-[0-9]{3}-[0-9]{2}", s);
    }
    
    private static String reemplazarNoAlfanumericos(String s){
        Pattern p = Pattern.compile("\\W");
        Matcher m = p.matcher(s);
        return m.replaceAll("_");
    }
    private static boolean esFormatoFecha(String s){
        String patronDia = "((0[1-9])|([1-2][0-9])|(3[01]))";
        String patronMes = "((0[1-9])|(1[0-2]))";
        String patronAnno = "[0-9]{4}";
        String patron = patronDia + "/" + patronMes +"/" + patronAnno;
        return Pattern.matches(patron, s);
    }
    
    private static boolean esIP(String s){        
        String patron = "((([01]?[0-9]?[0-9])|(2[0-4][0-9])|(25[0-5]))\\.){3}(([01]?[0-9]?[0-9])|(2[0-4][0-9])|(25[0-5]))";        
        //System.out.println(patron);       
        return Pattern.matches(patron, s);
    }
    
    private static String limpiarCadena(String s){
        Pattern p = Pattern.compile("[^a-zA-Z0-9]");
        Matcher m = p.matcher(s);
        return m.replaceAll("");
    }
    
    private static String limpiarIP(String s){        
        Pattern p = Pattern.compile("(\\b|\\D)0+([0-9])");
        Matcher m = p.matcher(s);
        return m.replaceAll("$1$2");
    }
}
